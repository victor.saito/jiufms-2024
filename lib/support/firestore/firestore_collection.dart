enum FirestoreCollection {
  auth('Auth'),
  modalities('modalities'),
  atleticas('atleticas'),
  matches('matches');

  final String value;

  const FirestoreCollection(this.value);
}
