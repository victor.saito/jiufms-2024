import 'package:cloud_firestore/cloud_firestore.dart';

import '../../models/match_model.dart';
import 'firestore_collection.dart';

abstract class FirestoreProviderProtocol {
  Future<Map<String, dynamic>> getFirstData(FirestoreCollection collection);
  Future<List<Map<String, dynamic>>> getList({
    required FirestoreCollection collection,
    String? orderedBy,
  });
  Future<List<MatchModel>> getMatchesByModality(String modalityId);
  Future<List<MatchModel>> getMatchesListOrdered({
    required FirestoreCollection collection,
    bool? hasEnded,
    String? filterStart,
    String? filterEnd,
  });
  Future<void> createMatch({
    required String date,
    required String location,
    required String modalityId,
    String? teamA,
    String? teamB,
    String? modality,
    String? observation,
    int pointsTeamA,
    int pointsTeamB,
    bool hasEnded,
  });
  Future<void> updateMatch({
    required String matchId,
    String? date,
    String? teamA,
    String? teamB,
    String? location,
    String? observation,
    String? modality,
    int pointsTeamA,
    int pointsTeamB,
    bool hasEnded,
  });
  Future<void> deleteMatch(String id);
}

class FirestoreProvider extends FirestoreProviderProtocol {
  static FirestoreProviderProtocol instance = FirestoreProvider._();

  static final _firestore = FirebaseFirestore.instance;

  FirestoreProvider._();

  // MARK: - Get

  @override
  Future<Map<String, dynamic>> getFirstData(FirestoreCollection collection) async {
    final querySnapshot = await _firestore.collection(collection.value).get();
    final data = <String, dynamic>{};

    for (final doc in querySnapshot.docs) {
      data[doc.id] = doc.data();
    }

    return data.values.first;
  }

  @override
  Future<List<Map<String, dynamic>>> getList({
    required FirestoreCollection collection,
    String? orderedBy,
  }) async {
    final firebaseCollection = _firestore.collection(collection.value);
    final querySnapshot =
        (orderedBy != null) ? await firebaseCollection.orderBy(orderedBy).get() : await firebaseCollection.get();

    return querySnapshot.docs.map((doc) => doc.data()).toList();
  }

  @override
  Future<List<MatchModel>> getMatchesListOrdered({
    required FirestoreCollection collection,
    bool? hasEnded,
    String? filterStart,
    String? filterEnd,
  }) async {
    final firebaseCollection = _firestore.collection(collection.value);
    final querySnapshot = (hasEnded != null)
        ? await firebaseCollection
            .where(
              'dateTime',
              isGreaterThan: filterStart,
              isLessThan: filterEnd,
            )
            .where(
              'hasEnded',
              isEqualTo: hasEnded,
            )
            .get()
        : await firebaseCollection.get();
    final map = querySnapshot.docs.map((doc) => doc.data()).toList();
    final list = map
        .map(
          (match) => MatchModel.fromMap(match),
        )
        .toList();

    list.sort((a, b) {
      return hasEnded ?? false ? b.dateTime.compareTo(a.dateTime) : a.dateTime.compareTo(b.dateTime);
    });

    return list;
  }

  @override
  Future<List<MatchModel>> getMatchesByModality(String modalityId) async {
    final querySnapshot = await _firestore
        .collection(FirestoreCollection.matches.value)
        .where(
          'modalityId',
          isEqualTo: modalityId,
        )
        .get();
    final matchesListMap = querySnapshot.docs.map((doc) => doc.data()).toList();

    final list = matchesListMap
        .map(
          (match) => MatchModel.fromMap(match),
        )
        .toList();

    list.sort((a, b) => a.dateTime.compareTo(b.dateTime));

    return list;
  }

  // MARK: - Create

  @override
  Future<void> createMatch({
    required String date,
    required String location,
    required String modalityId,
    String? teamA,
    String? teamB,
    String? modality,
    String? observation,
    int? pointsTeamA,
    int? pointsTeamB,
    bool hasEnded = false,
  }) async {
    final collection = _firestore.collection(FirestoreCollection.matches.value);
    final matchId = collection.doc().id;

    await collection.doc(matchId).set(
      {
        'matchId': matchId,
        'modalityId': modalityId,
        'dateTime': date,
        'location': location,
        'observation': observation,
        'teamAName': teamA,
        'teamBName': teamB,
        'pointsTeamA': pointsTeamA,
        'pointsTeamB': pointsTeamB,
        'hasEnded': hasEnded,
        'modalityName': modality,
      },
    );
  }

  // MARK: - Update

  @override
  Future<void> updateMatch({
    required String matchId,
    int? pointsTeamA,
    int? pointsTeamB,
    bool hasEnded = false,
    String? date,
    String? teamA,
    String? teamB,
    String? location,
    String? observation,
    String? modality,
  }) async {
    final collection = _firestore.collection(FirestoreCollection.matches.value);

    await collection.doc(matchId).update(
      {
        'dateTime': date,
        'location': location,
        'teamAName': teamA,
        'teamBName': teamB,
        'pointsTeamA': pointsTeamA,
        'pointsTeamB': pointsTeamB,
        'hasEnded': hasEnded,
        'modalityName': modality,
        'observation': observation,
      },
    );
  }

  // MARK: - Delete

  @override
  Future<void> deleteMatch(String id) async {
    await _firestore.collection(FirestoreCollection.matches.value).doc(id).delete();
  }
}
