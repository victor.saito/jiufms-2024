import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../features/modality/modality_view.dart';
import '../../models/match_model.dart';
import '../styles/app_colors.dart';
import '../utils/course.dart';

class MatchCard extends StatelessWidget {
  final int index;
  final MatchModel match;
  final ModalityViewModelProtocol? delegate;

  const MatchCard({required this.index, required this.match, this.delegate, super.key});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: AppColors.lightBlue,
      margin: const EdgeInsets.symmetric(vertical: 8),
      child: InkWell(
        onTap: () => delegate?.didTapMatch(index),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (delegate != null)
                    IconButton(
                      onPressed: () => delegate?.didTapDelete(index),
                      icon: const Icon(Icons.close_sharp, size: 32),
                    ),
                  Text(
                    match.modalityName,
                    style: TextStyle(
                      fontSize: match.teamAName != '' ? 16 : 24,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 16),
              if (match.teamAName != '' || match.teamBName != '')
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(_courseLogo(match.teamAName) ?? ''),
                            ),
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Text(
                            match.teamAName ?? '',
                            textAlign: match.hasEnded ? TextAlign.left : TextAlign.center,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ),
                        if (match.hasEnded)
                          Text(
                            ' ${match.pointsTeamA}',
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                            ),
                          )
                      ],
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(_courseLogo(match.teamBName) ?? ''),
                            ),
                          ),
                        ),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Text(
                            match.teamBName ?? '',
                            textAlign: match.hasEnded ? TextAlign.left : TextAlign.center,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                            ),
                          ),
                        ),
                        if (match.hasEnded)
                          Text(
                            ' ${match.pointsTeamB}',
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(DateFormat('dd/MM - HH:mm').format(match.dateTime)),
                  const SizedBox(width: 20),
                  const Icon(Icons.pin_drop_outlined),
                  const SizedBox(width: 8),
                  Flexible(
                    child: Text(
                      match.location,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      softWrap: false,
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 8),
              if (match.observation != null)
                Column(
                  children: [
                    Text(
                      match.observation ?? '',
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(height: 8),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }

  String? _courseLogo(String? course) {
    for (final element in Course.values) {
      if (element.name == course) {
        return element.assetPath;
      }
    }
    return null;
  }
}
