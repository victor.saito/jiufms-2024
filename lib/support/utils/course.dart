import '../styles/app_assets.dart';

enum Course {
  adm('Administração', AppAssets.adm),
  arq('Arquitetura/Geografia', AppAssets.arq),
  aaav('Artes Visuais', AppAssets.aaaav),
  aaabio('Biologia', AppAssets.aaabio),
  jorn('Comunicação', AppAssets.jorn),
  aaacomp('Computação', AppAssets.aaacompLogo),
  aaacont('Contábeis', AppAssets.aaacont),
  canaaa('Direito', AppAssets.canaaa),
  econo('Economia', AppAssets.econo),
  aaef('Educação Física', AppAssets.aaef),
  magaaa('Enfermagem', AppAssets.magaaa),
  eng('Engenharias', AppAssets.eng),
  aaacex('Exatas', AppAssets.aaacex),
  aaafarma('Farmácia', AppAssets.aaafarma),
  aaafisio('Fisioterapia', AppAssets.aaafisio),
  humanas('Humanas', AppAssets.humanas),
  aaalma('Letras', AppAssets.aaalma),
  med('Medicina', AppAssets.med),
  medved('Medicina Veterinária', AppAssets.medvet),
  nutri('Nutrição', AppAssets.nutri),
  odonto('Odontologia', AppAssets.odonto),
  aaaped('Pedagogia', AppAssets.aaaped),
  psicaaa('Psicologia', AppAssets.psicaaa),
  aaazoo('Zootecnia', AppAssets.aaazoo);

  final String name;
  final String assetPath;

  const Course(this.name, this.assetPath);
}
