import '../../../localization/localize.dart';
import '../../firestore/firestore_provider.dart';
import 'app_module.dart';
import 'service_locator.dart';

class CommonsModule extends AppModule {
  @override
  void registerDependencies() {
    ServiceLocator.registerLazySingleton<LocalizeProtocol>(Localize.new);

    ServiceLocator.registerSingleton<FirestoreProviderProtocol>(FirestoreProvider.instance);
  }
}
