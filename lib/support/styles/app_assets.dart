class AppAssets {
  AppAssets._();

  static const String _assetsPath = 'assets/images/';

  /// Images

  static const String imageLogo = '${_assetsPath}logo.PNG';

  static const String aaacompLogo = '${_assetsPath}aaacomp.PNG';
  static const String medvet = '${_assetsPath}medved.png';
  static const String adm = '${_assetsPath}adm.png';
  static const String odonto = '${_assetsPath}odonto.png';
  static const String jorn = '${_assetsPath}jorn.png';
  static const String arq = '${_assetsPath}arq.png';
  static const String aaaav = '${_assetsPath}aaaav.PNG';
  static const String aaabio = '${_assetsPath}aaabio.PNG';
  static const String aaacex = '${_assetsPath}aaacex.png';
  static const String aaafarma = '${_assetsPath}aaafarma.png';
  static const String aaafisio = '${_assetsPath}aaafisio.png';
  static const String med = '${_assetsPath}med.png';
  static const String nutri = '${_assetsPath}nutri.PNG';
  static const String aaaped = '${_assetsPath}aaaped.png';
  static const String aaazoo = '${_assetsPath}aaazoo.png';
  static const String aaef = '${_assetsPath}aaef.png';
  static const String canaaa = '${_assetsPath}canaaa.png';
  static const String aaacont = '${_assetsPath}aaacont.PNG';
  static const String eng = '${_assetsPath}eng.PNG';
  static const String econo = '${_assetsPath}econo.png';
  static const String magaaa = '${_assetsPath}magaaa.png';
  static const String aaalma = '${_assetsPath}aaalma.png';
  static const String psicaaa = '${_assetsPath}psicaaa.png';
  static const String humanas = '${_assetsPath}humanas.png';
}
