import 'package:flutter/material.dart';

class AppColors {
  static const Color white = Color.fromRGBO(255, 255, 255, 1);
  static const Color black = Color.fromRGBO(0, 0, 0, 1);
  static const Color primary = Color.fromRGBO(41, 185, 242, 1);
  static const Color secondary = Color.fromRGBO(67, 110, 115, 1);

  static const Color lightBlue = Color.fromRGBO(205, 237, 245, 1);
  static const Color red = Color.fromRGBO(239, 35, 35, 1);
}
