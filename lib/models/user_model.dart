class UserModel {
  String? userName;
  String? password;

  UserModel.fromMap(Map<String, dynamic> map)
      : userName = map['User'],
        password = map['Password'];
}
