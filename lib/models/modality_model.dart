class ModalityModel {
  String id;
  String name;

  ModalityModel.fromMap(Map<String, dynamic> map)
      : id = map['id'],
        name = map['name'];
}
