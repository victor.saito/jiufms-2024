import 'package:intl/intl.dart';

class MatchModel {
  bool hasEnded;
  int? pointsTeamA;
  int? pointsTeamB;
  String matchId;
  String modalityId;
  DateTime dateTime;
  String location;
  String? teamAName;
  String? teamBName;
  String modalityName;
  String? observation;

  MatchModel.fromMap(Map<String, dynamic> map)
      : hasEnded = map['hasEnded'],
        pointsTeamA = map['pointsTeamA'],
        pointsTeamB = map['pointsTeamB'],
        matchId = map['matchId'],
        modalityId = map['modalityId'],
        modalityName = map['modalityName'],
        dateTime = DateFormat('dd/MM - HH:mm').parse(map['dateTime']),
        location = map['location'],
        teamAName = map['teamAName'],
        teamBName = map['teamBName'],
        observation = map['observation'];
}
