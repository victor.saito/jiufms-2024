import '../../models/modality_model.dart';
import '../../support/firestore/firestore_collection.dart';
import '../../support/firestore/firestore_provider.dart';

class ModalitySelectionModelLogic {
  final FirestoreProviderProtocol firestore;

  ModalitySelectionModelLogic({required this.firestore});

  Future<List<ModalityModel>> fetchModalities() async {
    final modalitiesListMap = await firestore.getList(
      collection: FirestoreCollection.modalities,
      orderedBy: 'name',
    );

    return modalitiesListMap.map((modality) => ModalityModel.fromMap(modality)).toList();
  }
}
