import 'package:flutter/material.dart';

import '../../localization/localize.dart';
import '../../support/styles/app_colors.dart';
import '../../support/utils/service_locator/service_locator.dart';

abstract class ModalitySelectionViewModelProtocol with ChangeNotifier {
  List<String> get modalitiesNameList;
  bool get isLoading;

  void didTapClose();
  void didTapModality(int index);
}

class ModalitySelectionView extends StatelessWidget {
  final ModalitySelectionViewModelProtocol viewModel;

  const ModalitySelectionView({required this.viewModel, super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

    return Scaffold(
      appBar: AppBar(
        title: Text(l10n.modalitySelectionTitle),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () => viewModel.didTapClose(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: AnimatedBuilder(
          animation: viewModel,
          builder: (_, __) {
            if (viewModel.isLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(l10n.modalitySelectionDescription),
                ),
                Expanded(
                  child: GridView.builder(
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisSpacing: 16,
                      crossAxisSpacing: 16,
                    ),
                    itemCount: viewModel.modalitiesNameList.length,
                    itemBuilder: (_, index) {
                      return GestureDetector(
                        onTap: () => viewModel.didTapModality(index),
                        child: Container(
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: AppColors.lightBlue,
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Text(
                            viewModel.modalitiesNameList[index],
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
