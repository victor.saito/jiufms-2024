import '../../models/modality_model.dart';
import '../../support/firestore/firestore_provider.dart';
import 'modality_selection_model.dart';
import 'modality_selection_view_controller.dart';

class ModalitySelectionViewModel extends ModalitySelectionProtocol {
  final ModalitySelectionModelLogic modelLogic;

  List<ModalityModel> modalitiesList = [];

  ModalitySelectionViewModel({required FirestoreProviderProtocol firestore})
      : modelLogic = ModalitySelectionModelLogic(firestore: firestore);

  @override
  bool isLoading = true;

  @override
  List<String> modalitiesNameList = [];

  @override
  Future<void> loadModalities() async {
    modalitiesList = await modelLogic.fetchModalities();

    modalitiesNameList = modalitiesList.map((modality) => modality.name).toList();
    isLoading = false;
    notifyListeners();
  }

  @override
  void didTapClose() {
    onTapClose?.call();
  }

  @override
  void didTapModality(int index) {
    onTapModality?.call(modalitiesList[index]);
  }
}
