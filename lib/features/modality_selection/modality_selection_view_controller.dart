import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../models/modality_model.dart';
import '../../router/mobile_router.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'modality_selection_view.dart';

abstract class ModalitySelectionProtocol extends ModalitySelectionViewModelProtocol {
  VoidCallback? onTapClose;
  void Function(ModalityModel modalityName)? onTapModality;

  Future<void> loadModalities();
}

class ModalitySelectionViewController extends StatefulWidget {
  const ModalitySelectionViewController({super.key});

  @override
  State<ModalitySelectionViewController> createState() => _ModalitySelectionViewController();
}

class _ModalitySelectionViewController extends State<ModalitySelectionViewController> {
  final viewModel = ServiceLocator.get<ModalitySelectionProtocol>();

  @override
  void initState() {
    super.initState();
    viewModel.loadModalities();
    bind();
  }

  @override
  Widget build(BuildContext context) {
    return ModalitySelectionView(viewModel: viewModel);
  }

  void bind() {
    viewModel.onTapClose = () => context.pop();

    viewModel.onTapModality = (modality) => context.pushNamed(
          MobileRouter.modalityRoute,
          extra: modality,
        );
  }
}
