import '../../support/firestore/firestore_provider.dart';
import '../../support/utils/service_locator/app_module.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'modality_selection_view_controller.dart';
import 'modality_selection_view_model.dart';

class ModalitySelectionModule extends AppModule {
  @override
  void registerDependencies() {
    ServiceLocator.registerFactory<ModalitySelectionProtocol>(() {
      return ModalitySelectionViewModel(
        firestore: ServiceLocator.get<FirestoreProviderProtocol>(),
      );
    });
  }
}
