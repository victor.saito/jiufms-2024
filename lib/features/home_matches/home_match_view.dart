import 'package:flutter/material.dart';

import '../../models/match_model.dart';
import '../../support/components/match_card.dart';

abstract class HomeMatchViewModelProtocol with ChangeNotifier {
  List<MatchModel> get matchesList;
  bool get isLoading;
  DateTime? get selectedDate;
  List<DateTime> get availableDates;

  Future<void> loadMatches(DateTime? newDate);
}

class HomeMatchView extends StatelessWidget {
  final HomeMatchViewModelProtocol viewModel;

  const HomeMatchView({required this.viewModel, super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: AnimatedBuilder(
        animation: viewModel,
        builder: (_, __) {
          if (viewModel.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          return RefreshIndicator(
            onRefresh: () async {
              await viewModel.loadMatches(viewModel.selectedDate);
            },
            child: Column(
              children: [
                DropdownButton<DateTime>(
                  hint: const Text('Selecione uma data'),
                  value: viewModel.selectedDate,
                  onChanged: (newDate) {
                    if (newDate != null) {
                      viewModel.loadMatches(newDate);
                    }
                  },
                  items: viewModel.availableDates.map((date) {
                    return DropdownMenuItem<DateTime>(
                      value: date,
                      child: Text('${date.day}/0${date.month}'),
                    );
                  }).toList(),
                ),
                if (viewModel.matchesList.isEmpty)
                  const Center(
                    child: Text(
                      'Ainda não existem partidas para esta data!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                Expanded(
                  child: ListView.builder(
                    itemCount: viewModel.matchesList.length,
                    itemBuilder: (context, index) {
                      final match = viewModel.matchesList[index];
                      return MatchCard(
                        match: match,
                        index: index,
                      );
                    },
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
