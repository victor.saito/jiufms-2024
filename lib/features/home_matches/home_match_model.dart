import 'package:intl/intl.dart';

import '../../models/match_model.dart';
import '../../support/firestore/firestore_collection.dart';
import '../../support/firestore/firestore_provider.dart';

class HomeMatchModel {
  final FirestoreProviderProtocol firestore;

  HomeMatchModel({required this.firestore});

  final List<DateTime> availableDates = [
    DateTime(2024, 09, 06),
    DateTime(2024, 09, 07),
    DateTime(2024, 09, 08),
    DateTime(2024, 09, 14),
    DateTime(2024, 09, 15),
    DateTime(2024, 09, 21),
    DateTime(2024, 09, 22),
  ];

  DateTime getNextDateTime() {
    var supportDate = availableDates.first;

    for (final date in availableDates) {
      if (date.month < DateTime.now().month) {
        return supportDate;
      }

      supportDate = date;
      if (date.day >= DateTime.now().day) {
        break;
      }
    }

    return supportDate;
  }

  Future<List<MatchModel>> getFilteredMatches({
    required DateTime selectedDate,
    bool? hasEnded,
  }) async {
    final selectedDatePlusOne = selectedDate.add(const Duration(days: 1));

    return await firestore.getMatchesListOrdered(
      collection: FirestoreCollection.matches,
      hasEnded: hasEnded,
      filterStart: DateFormat('dd/MM').format(selectedDate),
      filterEnd: DateFormat('dd/MM').format(selectedDatePlusOne),
    );
  }
}
