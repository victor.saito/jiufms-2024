import '../../models/match_model.dart';
import '../../support/firestore/firestore_provider.dart';
import 'home_match_model.dart';
import 'home_match_view.dart';

class HomeMatchViewModel extends HomeMatchViewModelProtocol {
  final HomeMatchModel model;
  final bool? hasEnded;

  HomeMatchViewModel({required FirestoreProviderProtocol firestore, this.hasEnded})
      : model = HomeMatchModel(firestore: firestore);

  @override
  List<DateTime> get availableDates => model.availableDates;

  @override
  List<MatchModel> matchesList = [];

  @override
  bool isLoading = true;

  @override
  late DateTime selectedDate = model.getNextDateTime();

  @override
  Future<void> loadMatches(DateTime? newDate) async {
    selectedDate = newDate ?? model.getNextDateTime();
    matchesList = await model.getFilteredMatches(
      selectedDate: selectedDate,
      hasEnded: hasEnded,
    );

    isLoading = false;
    notifyListeners();
  }
}
