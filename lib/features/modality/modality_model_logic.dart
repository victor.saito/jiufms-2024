import '../../models/match_model.dart';
import '../../support/firestore/firestore_provider.dart';

class ModalityModelLogic {
  final FirestoreProviderProtocol firestore;

  ModalityModelLogic({required this.firestore});

  Future<List<MatchModel>> fetchMatchesByModality(String modalityId) async {
    return await firestore.getMatchesByModality(modalityId);
  }

  Future<void> deleteMatch(String matchId) async {
    await firestore.deleteMatch(matchId);
  }
}
