import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../localization/localize.dart';
import '../../models/match_model.dart';
import '../../models/modality_model.dart';
import '../../router/mobile_router.dart';
import '../../support/components/match_card.dart';
import '../../support/styles/app_colors.dart';
import '../../support/utils/service_locator/service_locator.dart';

abstract class ModalityViewModelProtocol with ChangeNotifier {
  List<MatchModel> get matchesList;
  bool get isLoading;

  Future<void> loadMatches();
  void didTapMatch(int index);
  void didTapDelete(int index);
}

class ModalityView extends StatelessWidget {
  final ModalityViewModelProtocol viewModel;
  final ModalityModel modality;

  const ModalityView({required this.modality, required this.viewModel, super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

    return Scaffold(
      appBar: AppBar(title: Text(modality.name)),
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppColors.lightBlue,
        child: const Icon(Icons.add),
        onPressed: () => context.pushNamed(
          MobileRouter.matchRoute,
          extra: (modality, null),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: AnimatedBuilder(
          animation: viewModel,
          builder: (_, __) {
            if (viewModel.isLoading) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }

            return RefreshIndicator(
              onRefresh: () async {
                await viewModel.loadMatches();
              },
              child: Column(
                children: [
                  viewModel.matchesList.isNotEmpty ? Text(l10n.modalityMatchesDescription) : const SizedBox.shrink(),
                  Expanded(
                    child: ListView.builder(
                      itemCount: viewModel.matchesList.length,
                      itemBuilder: (context, index) {
                        final match = viewModel.matchesList[index];
                        return MatchCard(
                          match: match,
                          index: index,
                          delegate: viewModel,
                        );
                      },
                    ),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
