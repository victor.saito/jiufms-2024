import '../../models/match_model.dart';
import '../../support/firestore/firestore_provider.dart';
import 'modality_model_logic.dart';
import 'modality_view_controller.dart';

class ModalityViewModel extends ModalityProtocol {
  final ModalityModelLogic modelLogic;
  final String modalityId;

  ModalityViewModel({
    required this.modalityId,
    required FirestoreProviderProtocol firestore,
  }) : modelLogic = ModalityModelLogic(firestore: firestore);

  @override
  bool isLoading = true;

  @override
  List<MatchModel> matchesList = [];

  @override
  Future<void> loadMatches() async {
    matchesList = await modelLogic.fetchMatchesByModality(modalityId);
    isLoading = false;
    notifyListeners();
  }

  @override
  Future<void> deleteMatch(int index) async {
    isLoading = true;
    notifyListeners();

    await modelLogic.deleteMatch(matchesList[index].matchId);
    await loadMatches();
  }

  @override
  void didTapMatch(int index) {
    onTapMatch?.call(matchesList[index]);
  }

  @override
  void didTapDelete(int index) {
    onTapDelete?.call(index);
  }
}
