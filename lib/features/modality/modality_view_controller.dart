import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../localization/localize.dart';
import '../../models/match_model.dart';
import '../../models/modality_model.dart';
import '../../router/mobile_router.dart';
import '../../support/styles/app_colors.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'modality_view.dart';

abstract class ModalityProtocol extends ModalityViewModelProtocol {
  void Function(int index)? onTapDelete;
  void Function(MatchModel match)? onTapMatch;

  Future<void> loadMatches();
  void deleteMatch(int index);
}

class ModalityViewController extends StatefulWidget {
  final ModalityModel modality;

  const ModalityViewController({required this.modality, super.key});

  @override
  State<ModalityViewController> createState() => _ModalityViewControllerState();
}

class _ModalityViewControllerState extends State<ModalityViewController> {
  final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;
  late ModalityProtocol viewModel;

  @override
  void initState() {
    super.initState();
    viewModel = ServiceLocator.get<ModalityProtocol>(param: widget.modality.id);
    bind();
    viewModel.loadMatches();
  }

  @override
  Widget build(BuildContext context) {
    return ModalityView(
      modality: widget.modality,
      viewModel: viewModel,
    );
  }

  void bind() {
    viewModel.onTapMatch = (match) {
      context.pushNamed(MobileRouter.matchRoute, extra: (widget.modality, match));
    };

    viewModel.onTapDelete = (index) {
      showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(l10n.deleteMatchAlertTitle),
            content: Text(l10n.deleteMatchAlertDescription),
            actionsAlignment: MainAxisAlignment.spaceAround,
            actions: [
              TextButton(
                child: Text(
                  l10n.deleteMatchAlertContinueButtonText,
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: AppColors.red,
                  ),
                ),
                onPressed: () {
                  viewModel.deleteMatch(index);
                  context.pop();
                },
              ),
              TextButton(
                child: Text(
                  l10n.deleteMatchAlertLeaveButtonText,
                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                onPressed: () => context.pop(),
              ),
            ],
          );
        },
      );
    };
  }
}
