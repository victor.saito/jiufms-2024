import '../../support/firestore/firestore_provider.dart';
import '../../support/utils/service_locator/app_module.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'modality_view_controller.dart';
import 'modality_view_model.dart';

class ModalityModule extends AppModule {
  @override
  void registerDependencies() {
    ServiceLocator.registerFactoryParam<ModalityProtocol, String>((modalityId) {
      return ModalityViewModel(
        firestore: ServiceLocator.get<FirestoreProviderProtocol>(),
        modalityId: modalityId,
      );
    });
  }
}
