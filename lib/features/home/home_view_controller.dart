import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../router/mobile_router.dart';
import '../../support/utils/service_locator/service_locator.dart';
import '../home_matches/home_match_view.dart';
import 'home_view.dart';

abstract class HomeProtocol extends HomeViewModelProtocol {
  VoidCallback? onBadgeTapped;
}

class HomeViewController extends StatefulWidget {
  const HomeViewController({super.key});

  @override
  State<HomeViewController> createState() => _HomeViewControllerState();
}

class _HomeViewControllerState extends State<HomeViewController> {
  final viewModel = ServiceLocator.get<HomeProtocol>();
  final calendarViewModel = ServiceLocator.get<HomeMatchViewModelProtocol>(param: false);
  final resultsViewModel = ServiceLocator.get<HomeMatchViewModelProtocol>(param: true);

  @override
  void initState() {
    super.initState();
    bind();
    calendarViewModel.loadMatches(null);
    resultsViewModel.loadMatches(null);
  }

  @override
  Widget build(BuildContext context) {
    return HomeView(
      viewModel: viewModel,
      calendarViewModel: calendarViewModel,
      resultsViewModel: resultsViewModel,
    );
  }

  void bind() {
    viewModel.onBadgeTapped = () => context.pushNamed(MobileRouter.adminLoginRoute);
  }
}
