import '../../support/firestore/firestore_provider.dart';
import '../../support/utils/service_locator/app_module.dart';
import '../../support/utils/service_locator/service_locator.dart';
import '../home_matches/home_match_view.dart';
import '../home_matches/home_match_view_model.dart';
import 'home_view_controller.dart';
import 'home_view_model.dart';

class HomeModule extends AppModule {
  @override
  void registerDependencies() {
    ServiceLocator.registerFactory<HomeProtocol>(() {
      return HomeViewModel();
    });

    ServiceLocator.registerFactoryParam<HomeMatchViewModelProtocol, bool>((param) {
      return HomeMatchViewModel(
        hasEnded: param,
        firestore: ServiceLocator.get<FirestoreProviderProtocol>(),
      );
    });
  }
}
