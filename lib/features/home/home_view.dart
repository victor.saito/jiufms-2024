import 'package:flutter/material.dart';

import '../../localization/localize.dart';
import '../../support/utils/service_locator/service_locator.dart';
import '../about/about_view.dart';
import '../home_matches/home_match_view.dart';

abstract class HomeViewModelProtocol with ChangeNotifier {
  int get currentIndex;
  String get currentTitle;

  void onTabTapped(int index);
  void didBadgeTapped();
}

class HomeView extends StatelessWidget {
  final HomeViewModelProtocol viewModel;
  final HomeMatchViewModelProtocol calendarViewModel;
  final HomeMatchViewModelProtocol resultsViewModel;
  final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

  HomeView({
    required this.viewModel,
    required this.calendarViewModel,
    required this.resultsViewModel,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListenableBuilder(
      listenable: viewModel,
      builder: (_, __) {
        return Scaffold(
          appBar: AppBar(
            title: Text(viewModel.currentTitle),
            actions: [
              IconButton(
                icon: const Icon(Icons.badge_outlined),
                onPressed: () => viewModel.didBadgeTapped(),
              ),
            ],
          ),
          body: Builder(
            builder: (_) {
              switch (viewModel.currentIndex) {
                case 1:
                  return HomeMatchView(viewModel: resultsViewModel);
                case 2:
                  return const AboutView();
                default:
                  return HomeMatchView(viewModel: calendarViewModel);
              }
            },
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: viewModel.currentIndex,
            onTap: viewModel.onTabTapped,
            items: [
              BottomNavigationBarItem(
                icon: const Icon(Icons.calendar_today),
                label: l10n.homeCalendar,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.workspace_premium_outlined),
                label: l10n.homeResults,
              ),
              BottomNavigationBarItem(
                icon: const Icon(Icons.info_outline),
                label: l10n.homeAbout,
              ),
            ],
          ),
        );
      },
    );
  }
}
