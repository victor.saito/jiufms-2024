import '../../localization/localize.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'home_view_controller.dart';

class HomeViewModel extends HomeProtocol {
  int _currentIndex = 2;
  String? _currentTitle;

  final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

  @override
  int get currentIndex => _currentIndex;

  @override
  String get currentTitle => _currentTitle ?? l10n.homeCalendar;

  @override
  void onTabTapped(int index) {
    _currentIndex = index;
    switch (index) {
      case 0:
        _currentTitle = l10n.homeCalendar;
      case 1:
        _currentTitle = l10n.homeResults;
      case 2:
        _currentTitle = l10n.homeAbout;
    }
    notifyListeners();
  }

  @override
  void didBadgeTapped() {
    onBadgeTapped?.call();
  }
}
