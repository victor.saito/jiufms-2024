import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_gen/gen_l10n/localization.dart';

import '../../support/firestore/firestore_provider.dart';
import 'admin_login_model.dart';
import 'admin_login_view_controller.dart';

class AdminLoginViewModel extends AdminLoginProtocol {
  final AdminLoginModel model;
  final Localization l10n;

  AdminLoginViewModel({
    required this.l10n,
    required FirestoreProviderProtocol firestore,
  }) : model = AdminLoginModel(firestore: firestore);

  @override
  bool isPasswordHidden = true;

  @override
  bool isLoading = false;

  @override
  Future<void> didTapLogin({String user = '', String password = ''}) async {
    isLoading = true;
    notifyListeners();

    try {
      await model.signInWithFirebase(user, password);
      onExecuteLogin?.call();
    } on FirebaseAuthException catch (_) {
      onFailedLogin?.call();
    }

    isLoading = false;
    notifyListeners();
  }

  @override
  String? validateEmptyUser(String? user) {
    if (user == null || user.isEmpty) {
      return l10n.loginUserValidationDescription;
    }
    return null;
  }

  @override
  String? validateEmptyPassword(String? password) {
    if (password == null || password.isEmpty) {
      return l10n.loginUserValidationDescription;
    }
    return null;
  }

  @override
  void togglePassword() {
    isPasswordHidden = !isPasswordHidden;
    notifyListeners();
  }
}
