import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../localization/localize.dart';
import '../../router/mobile_router.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'admin_login_view.dart';

abstract class AdminLoginProtocol extends AdminLoginViewModelProtocol {
  VoidCallback? onExecuteLogin;
  VoidCallback? onFailedLogin;
}

class AdminLoginViewController extends StatefulWidget {
  const AdminLoginViewController({super.key});

  @override
  State<AdminLoginViewController> createState() => _AdminLoginViewControllerState();
}

class _AdminLoginViewControllerState extends State<AdminLoginViewController> {
  final viewModel = ServiceLocator.get<AdminLoginProtocol>();
  final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

  @override
  void initState() {
    super.initState();
    bind();
  }

  @override
  Widget build(BuildContext context) {
    return AdminLoginView(viewModel: viewModel);
  }

  void bind() {
    viewModel.onExecuteLogin = () {
      context.pushNamed(MobileRouter.modalitySelectionRoute);
    };

    viewModel.onFailedLogin = () {
      showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(l10n.defaultAlertTitle),
            content: Text(l10n.alertLoginDescription),
            actionsAlignment: MainAxisAlignment.center,
            actions: [
              TextButton(
                child: Text(l10n.defaultOkButtonText),
                onPressed: () => context.pop(),
              ),
            ],
          );
        },
      );
    };
  }
}
