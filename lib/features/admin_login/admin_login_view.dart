import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../localization/localize.dart';
import '../../support/utils/service_locator/service_locator.dart';

abstract class AdminLoginViewModelProtocol with ChangeNotifier {
  bool get isPasswordHidden;
  bool get isLoading;

  void togglePassword();
  void didTapLogin({String user, String password});
  String? validateEmptyUser(String? user);
  String? validateEmptyPassword(String? password);
}

class AdminLoginView extends StatelessWidget {
  final AdminLoginViewModelProtocol viewModel;

  const AdminLoginView({required this.viewModel, super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final usernameController = TextEditingController();
    final passwordController = TextEditingController();

    final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

    return Scaffold(
      appBar: AppBar(
        title: Text(l10n.loginTitle),
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () => context.pop(),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: formKey,
          child: AnimatedBuilder(
              animation: viewModel,
              builder: (context, widget) {
                if (viewModel.isLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                return Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Spacer(flex: 2),
                    Text(
                      l10n.loginDescription,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Spacer(flex: 2),
                    TextFormField(
                      controller: usernameController,
                      validator: viewModel.validateEmptyUser,
                      decoration: InputDecoration(
                        labelText: l10n.loginUserInputText,
                        border: const OutlineInputBorder(),
                      ),
                    ),
                    const SizedBox(height: 16),
                    TextFormField(
                      controller: passwordController,
                      obscureText: viewModel.isPasswordHidden,
                      validator: viewModel.validateEmptyPassword,
                      decoration: InputDecoration(
                        labelText: l10n.loginPasswordInputText,
                        border: const OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(
                            viewModel.isPasswordHidden ? Icons.visibility : Icons.visibility_off,
                          ),
                          onPressed: () {
                            viewModel.togglePassword();
                          },
                        ),
                      ),
                    ),
                    const Spacer(),
                    ElevatedButton(
                      onPressed: () {
                        if (formKey.currentState?.validate() ?? false) {
                          viewModel.didTapLogin(
                            user: usernameController.text,
                            password: passwordController.text,
                          );
                        }
                      },
                      child: Text(l10n.loginEnterButtonText),
                    ),
                    const Spacer(),
                  ],
                );
              }),
        ),
      ),
    );
  }
}
