import '../../localization/localize.dart';
import '../../support/firestore/firestore_provider.dart';
import '../../support/utils/service_locator/app_module.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'admin_login_view_controller.dart';
import 'admin_login_view_model.dart';

class AdminLoginModule extends AppModule {
  @override
  void registerDependencies() {
    ServiceLocator.registerFactory<AdminLoginProtocol>(() {
      return AdminLoginViewModel(
        l10n: ServiceLocator.get<LocalizeProtocol>().l10n,
        firestore: ServiceLocator.get<FirestoreProviderProtocol>(),
      );
    });
  }
}
