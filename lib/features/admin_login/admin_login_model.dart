import 'package:firebase_auth/firebase_auth.dart';

import '../../models/user_model.dart';
import '../../support/firestore/firestore_collection.dart';
import '../../support/firestore/firestore_provider.dart';

class AdminLoginModel {
  final FirestoreProviderProtocol firestore;

  AdminLoginModel({required this.firestore});

  Future<void> signInWithFirebase(String email, String password) async {
    await FirebaseAuth.instance.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<bool> isValidLogin(String user, String password) async {
    final loginMap = <String, dynamic>{
      'Password': password,
      'User': user,
    };

    final userMap = await firestore.getFirstData(FirestoreCollection.auth);

    final firstUserName = UserModel.fromMap(userMap).userName;
    final firstUserPassword = UserModel.fromMap(userMap).password;
    final secondUserName = UserModel.fromMap(loginMap).userName;
    final secondUserPassword = UserModel.fromMap(loginMap).password;

    return firstUserName == secondUserName && firstUserPassword == secondUserPassword;
  }
}
