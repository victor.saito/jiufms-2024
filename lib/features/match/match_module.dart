import '../../models/match_model.dart';
import '../../models/modality_model.dart';
import '../../support/firestore/firestore_provider.dart';
import '../../support/utils/service_locator/app_module.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'match_view_controller.dart';
import 'match_view_model.dart';

class MatchModule extends AppModule {
  @override
  void registerDependencies() {
    ServiceLocator.registerFactoryParam<MatchProtocol, (ModalityModel?, MatchModel?)>((matchArgument) {
      return MatchViewModel(
        modality: matchArgument.$1,
        matchData: matchArgument.$2,
        firestore: ServiceLocator.get<FirestoreProviderProtocol>(),
      );
    });
  }
}
