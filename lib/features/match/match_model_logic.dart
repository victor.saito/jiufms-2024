import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:intl/intl.dart';

import '../../models/match_model.dart';
import '../../models/modality_model.dart';
import '../../support/firestore/firestore_collection.dart';
import '../../support/firestore/firestore_provider.dart';

class MatchModelLogic {
  final FirestoreProviderProtocol firestore;

  MatchModelLogic({required this.firestore});

  Future<List<String>> fetchTeamsList() async {
    final atleticasMap = await firestore.getFirstData(FirestoreCollection.atleticas);
    final teams = (atleticasMap[FirestoreCollection.atleticas.value] as List<dynamic>).cast<String>();
    teams.add('');
    return teams;
  }

  bool checkIsIndividual(ModalityModel? modality) {
    final individualId = FirebaseRemoteConfig.instance.getString('individual');
    return modality?.id == individualId;
  }

  String formatDateTime(DateTime dateTime) {
    return DateFormat('dd/MM - HH:mm').format(dateTime);
  }

  Future<void> saveMatch({
    required MatchModel? matchData,
    required String? lastDateTime,
    required String? selectedATeam,
    required String? selectedBTeam,
    required String location,
    required String pointsTeamA,
    required String pointsTeamB,
    required bool hasEnded,
    required String? modalityName,
    required ModalityModel? modality,
    required String? observation,
  }) async {
    final aPoints = pointsTeamA.isEmpty ? 0 : int.parse(pointsTeamA);
    final bPoints = pointsTeamB.isEmpty ? 0 : int.parse(pointsTeamB);

    if (matchData?.matchId != null) {
      await firestore.updateMatch(
        date: lastDateTime,
        teamA: selectedATeam,
        teamB: selectedBTeam,
        location: location,
        observation: observation,
        modality: modalityName == '' ? modality?.name : modalityName,
        pointsTeamA: aPoints,
        pointsTeamB: bPoints,
        hasEnded: hasEnded,
        matchId: matchData!.matchId,
      );
    } else {
      await firestore.createMatch(
        date: lastDateTime ?? '',
        teamA: selectedATeam ?? '',
        teamB: selectedBTeam ?? '',
        location: location,
        observation: observation,
        modality: modalityName == '' ? modality?.name : modalityName,
        pointsTeamA: aPoints,
        pointsTeamB: bPoints,
        modalityId: modality?.id ?? '',
        hasEnded: hasEnded,
      );
    }
  }
}
