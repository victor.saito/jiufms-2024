import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';

import '../../localization/localize.dart';
import '../../models/match_model.dart';
import '../../models/modality_model.dart';
import '../../router/mobile_router.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'match_view.dart';

abstract class MatchProtocol extends MatchViewModelProtocol {
  VoidCallback? onTapSave;
  Future<String?> Function()? onTapDateTime;

  Future<void> loadMatchData();
}

class MatchViewController extends StatefulWidget {
  final ModalityModel? modality;
  final MatchModel? matchData;

  const MatchViewController({this.matchData, this.modality, super.key});

  @override
  State<MatchViewController> createState() => _MatchViewControllerState();
}

class _MatchViewControllerState extends State<MatchViewController> {
  late MatchProtocol viewModel;
  final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

  @override
  void initState() {
    super.initState();
    viewModel = ServiceLocator.get<MatchProtocol>(
      param: (widget.modality, widget.matchData),
    );
    bind();
  }

  @override
  Widget build(BuildContext context) {
    viewModel.loadMatchData();
    return MatchView(viewModel: viewModel, matchData: widget.matchData);
  }

  void bind() {
    viewModel.onTapDateTime = () async {
      final pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2024),
        lastDate: DateTime(2024, 12),
      );

      if (pickedDate != null) {
        final pickedTime = await showTimePicker(
          context: context,
          initialTime: TimeOfDay.now(),
        );

        if (pickedTime != null) {
          final fullDateTime = DateTime(
            pickedDate.year,
            pickedDate.month,
            pickedDate.day,
            pickedTime.hour,
            pickedTime.minute,
          );
          return DateFormat('dd/MM - HH:mm').format(fullDateTime);
        }
      }

      return null;
    };

    viewModel.onTapSave = () {
      showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text(l10n.editMatchAlertTitle),
            content: Text(l10n.editMatchAlertDescription),
            actionsAlignment: MainAxisAlignment.spaceAround,
            actions: [
              TextButton(
                child: Text(
                  l10n.editMatchAlertContinueButtonText,
                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                onPressed: () => context.pop(),
              ),
              TextButton(
                child: Text(
                  l10n.editMatchAlertLeaveButtonText,
                  style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                onPressed: () {
                  Navigator.of(context).popUntil(ModalRoute.withName(MobileRouter.modalitySelectionRoute));
                },
              ),
            ],
          );
        },
      );
    };
  }
}
