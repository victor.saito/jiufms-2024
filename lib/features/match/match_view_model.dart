import '../../localization/localize.dart';
import '../../models/match_model.dart';
import '../../models/modality_model.dart';
import '../../support/firestore/firestore_provider.dart';
import '../../support/utils/service_locator/service_locator.dart';
import 'match_model_logic.dart';
import 'match_view_controller.dart';

class MatchViewModel extends MatchProtocol {
  final MatchModelLogic modelLogic;
  final ModalityModel? modality;
  final MatchModel? matchData;

  final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

  bool _isFormValid = false;
  bool _isIndividual = false;
  String? _lastDateTime;
  String? _localText;
  String? _observationText;
  String? _modalityText;
  String? _selectedATeam;
  String? _selectedBTeam;
  String? _pointsTeamA;
  String? _pointsTeamB;
  List<String> _teamsList = [];

  MatchViewModel({
    required FirestoreProviderProtocol firestore,
    required this.modality,
    this.matchData,
  }) : modelLogic = MatchModelLogic(firestore: firestore);

  @override
  bool get isFormValid => _isFormValid;

  @override
  bool get isIndividual => _isIndividual;

  @override
  List<String> get teamsList => _teamsList;

  @override
  String? get selectedATeam => _selectedATeam;

  @override
  String? get selectedBTeam => _selectedBTeam;

  @override
  String? get dateText => _lastDateTime;

  @override
  String? get localText => _localText;

  @override
  String? get observationText => _observationText;

  @override
  String? get modalityText => _modalityText;

  @override
  String? get pointsTeamA => _pointsTeamA;

  @override
  String? get pointsTeamB => _pointsTeamB;

  @override
  Future<void> loadMatchData() async {
    _teamsList = await modelLogic.fetchTeamsList();
    _isIndividual = modelLogic.checkIsIndividual(modality);

    _lastDateTime = modelLogic.formatDateTime(matchData?.dateTime ?? DateTime.now());
    _localText = matchData?.location;
    _observationText = matchData?.observation;
    _modalityText = matchData?.modalityName;
    _selectedATeam = matchData?.teamAName;
    _selectedBTeam = matchData?.teamBName;
    _pointsTeamA = matchData?.pointsTeamA.toString();
    _pointsTeamB = matchData?.pointsTeamB.toString();
    _isFormValid = matchData != null;

    notifyListeners();
  }

  @override
  void validateForm(bool isFormValid) {
    _isFormValid = isFormValid;
    notifyListeners();
  }

  @override
  Future<String> didTapDateTime() async {
    _lastDateTime = await onTapDateTime?.call() ?? _lastDateTime;
    return _lastDateTime ?? '';
  }

  @override
  String? validateNotEmpty(String? value) {
    _isFormValid = value != null && value.isNotEmpty;
    return value == null || value.isEmpty ? l10n.editMatchHintText : null;
  }

  @override
  void setSelectedATeam(String? team) {
    _selectedATeam = team;
    notifyListeners();
  }

  @override
  void setSelectedBTeam(String? team) {
    _selectedBTeam = team;
    notifyListeners();
  }

  @override
  Future<void> saveMatch({
    required String location,
    String pointsTeamA = '0',
    String pointsTeamB = '0',
    bool hasEnded = false,
    String? modalityName,
    String? observation,
  }) async {
    await modelLogic.saveMatch(
      matchData: matchData,
      lastDateTime: _lastDateTime,
      selectedATeam: _selectedATeam,
      selectedBTeam: _selectedBTeam,
      location: location,
      pointsTeamA: pointsTeamA,
      pointsTeamB: pointsTeamB,
      hasEnded: hasEnded,
      modalityName: modalityName,
      modality: modality,
      observation: observation,
    );

    onTapSave?.call();
    _clearData();
  }

  void _clearData() {
    setSelectedATeam('');
    setSelectedBTeam('');

    _isFormValid = false;
    notifyListeners();
  }
}
