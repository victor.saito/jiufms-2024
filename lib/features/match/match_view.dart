import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../localization/localize.dart';
import '../../models/match_model.dart';
import '../../support/styles/app_colors.dart';
import '../../support/utils/service_locator/service_locator.dart';

abstract class MatchViewModelProtocol with ChangeNotifier {
  bool get isFormValid;
  bool get isIndividual;
  List<String> get teamsList;
  String? get selectedATeam;
  String? get selectedBTeam;
  String? get dateText;
  String? get localText;
  String? get modalityText;
  String? get pointsTeamA;
  String? get pointsTeamB;
  String? get observationText;

  void setSelectedATeam(String? team);
  void setSelectedBTeam(String? team);
  void validateForm(bool isFormValid);
  String? validateNotEmpty(String? value);
  Future<String> didTapDateTime();
  Future<void> saveMatch({
    required String location,
    String pointsTeamA,
    String pointsTeamB,
    String modalityName,
    bool hasEnded,
    String? observation,
  });
}

class MatchView extends StatelessWidget {
  final MatchViewModelProtocol viewModel;
  final MatchModel? matchData;

  const MatchView({required this.viewModel, this.matchData, super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    final dateTimeController = TextEditingController();
    final locationController = TextEditingController();
    final modalityController = TextEditingController();
    final pointsTeamAController = TextEditingController();
    final pointsTeamBController = TextEditingController();
    final observationTextController = TextEditingController();
    final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

    dateTimeController.text = DateFormat('dd/MM - HH:mm').format(matchData?.dateTime ?? DateTime.now());
    locationController.text = matchData?.location ?? '';
    modalityController.text = matchData?.modalityName ?? '';
    pointsTeamAController.text = matchData?.pointsTeamA.toString() ?? '';
    pointsTeamBController.text = matchData?.pointsTeamB.toString() ?? '';
    observationTextController.text = matchData?.observation ?? '';

    return Scaffold(
      appBar: AppBar(title: Text(l10n.editMatchTitle)),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Form(
          key: formKey,
          onChanged: () {
            viewModel.validateForm(formKey.currentState?.validate() ?? false);
          },
          child: AnimatedBuilder(
            animation: viewModel,
            builder: (_, __) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const SizedBox(height: 32),
                  TextFormField(
                    readOnly: true,
                    controller: dateTimeController,
                    validator: viewModel.validateNotEmpty,
                    onTap: () async {
                      dateTimeController.text = await viewModel.didTapDateTime();
                    },
                    decoration: InputDecoration(
                      labelText: l10n.editMatchDateTimeInputTitle,
                      border: const OutlineInputBorder(),
                      suffixIcon: const Icon(Icons.calendar_today),
                    ),
                  ),
                  const SizedBox(height: 16),
                  TextFormField(
                    controller: locationController,
                    validator: viewModel.validateNotEmpty,
                    onChanged: (value) => locationController.text = value,
                    decoration: InputDecoration(
                      labelText: l10n.editMatchLocalInputTitle,
                      border: const OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 16),
                  if (!viewModel.isIndividual)
                    Row(
                      children: [
                        Expanded(
                          child: DropdownButtonFormField<String>(
                            value: viewModel.selectedATeam ?? matchData?.teamAName,
                            onChanged: (newValue) {
                              viewModel.setSelectedATeam(newValue);
                            },
                            items: viewModel.teamsList.map((team) {
                              return DropdownMenuItem<String>(
                                value: team,
                                child: Text(team),
                              );
                            }).toList(),
                            decoration: InputDecoration(
                              labelText: l10n.editMatchATeamInputTitle,
                              border: const OutlineInputBorder(),
                            ),
                            validator: viewModel.validateNotEmpty,
                          ),
                        ),
                        const SizedBox(width: 16),
                        SizedBox(
                          width: 120,
                          child: TextFormField(
                            controller: pointsTeamAController,
                            keyboardType: TextInputType.number,
                            onChanged: (value) => pointsTeamAController.text = value,
                            decoration: InputDecoration(
                              labelText: l10n.editMatchPointsInputTitle,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  if (viewModel.isIndividual)
                    TextFormField(
                      controller: modalityController,
                      validator: viewModel.validateNotEmpty,
                      onChanged: (value) => modalityController.text = value,
                      decoration: InputDecoration(
                        labelText: l10n.editMatchModalityInputTitle,
                        border: const OutlineInputBorder(),
                      ),
                    ),
                  const SizedBox(height: 16),
                  if (!viewModel.isIndividual)
                    Row(
                      children: [
                        Expanded(
                          child: DropdownButtonFormField<String>(
                            value: viewModel.selectedBTeam ?? matchData?.teamBName,
                            onChanged: (newValue) {
                              viewModel.setSelectedBTeam(newValue);
                            },
                            items: viewModel.teamsList.map((team) {
                              return DropdownMenuItem<String>(
                                value: team,
                                child: Text(team),
                              );
                            }).toList(),
                            decoration: InputDecoration(
                              labelText: l10n.editMatchBTeamInputTitle,
                              border: const OutlineInputBorder(),
                            ),
                            validator: viewModel.validateNotEmpty,
                          ),
                        ),
                        const SizedBox(width: 16),
                        SizedBox(
                          width: 120,
                          child: TextFormField(
                            controller: pointsTeamBController,
                            onChanged: (value) => pointsTeamBController.text = value,
                            keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                              labelText: l10n.editMatchPointsInputTitle,
                              border: const OutlineInputBorder(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  const SizedBox(height: 16),
                  TextFormField(
                    controller: observationTextController,
                    onChanged: (value) => observationTextController.text = value,
                    decoration: const InputDecoration(
                      labelText: 'Observações',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 40),
                  ElevatedButton(
                    onPressed: viewModel.isFormValid
                        ? () {
                            viewModel.saveMatch(
                                location: locationController.text,
                                pointsTeamA: pointsTeamAController.text,
                                pointsTeamB: pointsTeamBController.text,
                                modalityName: modalityController.text,
                                observation: observationTextController.text);
                            locationController.text = '';
                            modalityController.text = '';
                            dateTimeController.text = '';
                            pointsTeamAController.text = '';
                            pointsTeamBController.text = '';
                            observationTextController.text = '';
                          }
                        : null,
                    child: Text(l10n.editMatchAddButtonText),
                  ),
                  const SizedBox(height: 40),
                  ElevatedButton(
                    style: const ButtonStyle(backgroundColor: WidgetStatePropertyAll(AppColors.secondary)),
                    onPressed: viewModel.isFormValid
                        ? () {
                            viewModel.saveMatch(
                              location: locationController.text,
                              modalityName: modalityController.text,
                              pointsTeamA: pointsTeamAController.text,
                              pointsTeamB: pointsTeamBController.text,
                              hasEnded: true,
                              observation: observationTextController.text,
                            );
                            locationController.text = '';
                            modalityController.text = '';
                            dateTimeController.text = '';
                            pointsTeamAController.text = '';
                            pointsTeamBController.text = '';
                            observationTextController.text = '';
                          }
                        : null,
                    child: Text(l10n.editMatchAddAndEndButtonText),
                  ),
                  const SizedBox(height: 32),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
