import 'dart:async';

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../../router/mobile_router.dart';
import 'splash_screen_view.dart';

class SplashScreenViewController extends StatefulWidget {
  const SplashScreenViewController({super.key});

  @override
  State<SplashScreenViewController> createState() => _SplashScreenViewControllerState();
}

class _SplashScreenViewControllerState extends State<SplashScreenViewController> {
  @override
  void initState() {
    super.initState();
    _runTimer();
  }

  @override
  Widget build(BuildContext context) {
    return const SplashScreenView();
  }

  void _runTimer() {
    Timer(const Duration(seconds: 2), () {
      context.goNamed(MobileRouter.homeRoute);
    });
  }
}
