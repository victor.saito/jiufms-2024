import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../localization/localize.dart';
import '../../support/utils/service_locator/service_locator.dart';

class AboutView extends StatelessWidget {
  String? get forms {
    return FirebaseRemoteConfig.instance.getString('forms');
  }

  String? get privacy {
    return FirebaseRemoteConfig.instance.getString('privacy');
  }

  String? get jiufmsRegulation {
    return FirebaseRemoteConfig.instance.getString('regulation');
  }

  const AboutView({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = ServiceLocator.get<LocalizeProtocol>().l10n;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          const SizedBox(height: 20),
          Text(
            l10n.aboutDescription,
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
          const Spacer(),
          if (forms != '' && forms != null)
            ElevatedButton(
              onPressed: () => _launchUrl(forms ?? ''),
              child: Text(l10n.aboutFormInputTitle),
            ),
          if (jiufmsRegulation != '' && jiufmsRegulation != null)
            TextButton(
              onPressed: () => _launchUrl(jiufmsRegulation ?? ''),
              child: Text(l10n.aboutRegulationInputTitle),
            ),
          if (privacy != '' && privacy != null)
            TextButton(
              onPressed: () => _launchUrl(privacy ?? ''),
              child: Text(l10n.aboutPrivacyInputTitle),
            ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }

  void _launchUrl(String urlLink) {
    final url = Uri.parse(urlLink);
    launchUrl(url, mode: LaunchMode.externalApplication);
  }
}
