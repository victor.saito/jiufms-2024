import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import '../features/admin_login/admin_login_view_controller.dart';
import '../features/home/home_view_controller.dart';
import '../features/match/match_view_controller.dart';
import '../features/modality/modality_view_controller.dart';
import '../features/modality_selection/modality_selection_view_controller.dart';
import '../features/splash/splash_screen_view_controller.dart';
import '../models/match_model.dart';
import '../models/modality_model.dart';

abstract class AppRouter {
  List<RouteBase> get routes;
}

class MobileRouter {
  static final GlobalKey<NavigatorState> rootNavigatorKey = GlobalKey<NavigatorState>();

  static const String splashRoute = '/splash';
  static const String homeRoute = '/home';
  static const String adminLoginRoute = 'admin_login';
  static const String modalitySelectionRoute = 'modality_selection';
  static const String modalityRoute = 'modality';
  static const String matchRoute = 'match';

  static final GoRouter router = GoRouter(
    navigatorKey: rootNavigatorKey,
    initialLocation: splashRoute,
    routes: <RouteBase>[
      GoRoute(
        path: splashRoute,
        name: splashRoute,
        builder: (_, __) => const SplashScreenViewController(),
      ),
      GoRoute(
        path: homeRoute,
        name: homeRoute,
        builder: (_, __) => const HomeViewController(),
        routes: [
          GoRoute(
            path: adminLoginRoute,
            name: adminLoginRoute,
            builder: (_, __) => const AdminLoginViewController(),
          ),
          GoRoute(
            path: modalitySelectionRoute,
            name: modalitySelectionRoute,
            builder: (_, __) => const ModalitySelectionViewController(),
            routes: [
              GoRoute(
                path: modalityRoute,
                name: modalityRoute,
                builder: (_, state) {
                  final modality = state.extra as ModalityModel;
                  return ModalityViewController(modality: modality);
                },
                routes: [
                  GoRoute(
                    path: matchRoute,
                    name: matchRoute,
                    builder: (_, state) {
                      final extra = state.extra as (ModalityModel, MatchModel?);
                      return MatchViewController(modality: extra.$1, matchData: extra.$2);
                    },
                  )
                ],
              )
            ],
          ),
        ],
      ),
    ],
  );
}
